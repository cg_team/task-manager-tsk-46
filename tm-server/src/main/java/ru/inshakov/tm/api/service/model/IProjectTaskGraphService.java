package ru.inshakov.tm.api.service.model;

import ru.inshakov.tm.model.TaskGraph;

import java.util.List;

public interface IProjectTaskGraphService {

    List<TaskGraph> findTaskByProjectId(String userId, final String projectId);

    void bindTaskById(String userId, final String taskId, final String projectId);

    void unbindTaskById(String userId, final String taskId);

    void removeProjectById(String userId, final String projectId);

    void removeProjectByIndex(String userId, final Integer index);

    void removeProjectByName(String userId, final String name);

}
