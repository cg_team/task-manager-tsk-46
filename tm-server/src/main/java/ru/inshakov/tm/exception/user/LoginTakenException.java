package ru.inshakov.tm.exception.user;

import org.jetbrains.annotations.Nullable;

public final class LoginTakenException extends RuntimeException {

    public LoginTakenException(@Nullable final String login) {
        super("Error! Login ``" + login + "`` is already taken...");
    }

}
