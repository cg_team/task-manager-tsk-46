package ru.inshakov.tm.exception.empty;

import org.jetbrains.annotations.NotNull;

public final class EmptyIdException extends RuntimeException {

    public EmptyIdException() {
        super("Error! Id is empty...");
    }

    @NotNull
    public EmptyIdException(String value) {
        super("Error. " + value + " id is empty");
    }

}
